package main

import "mgnlboot/cmd"

var version = "~development"

//go:generate go run doc/gen.go

func main() {
	cmd.Execute(version)
}
