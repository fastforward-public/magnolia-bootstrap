package cmd

import (
	"testing"
	"time"
)

func TestActivationLock_IsLocked(t *testing.T) {
	type fields struct {
		locks map[string]*time.Timer
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "locked",
			fields: fields{
				locks: map[string]*time.Timer{
					"b": time.NewTimer(time.Minute), // Should be enough for the test.
				},
			},
			want: true,
		},
		{
			name: "unlocked",
			fields: fields{
				locks: map[string]*time.Timer{},
			},
			want: false,
		},
		{
			name: "all locked",
			fields: fields{
				locks: map[string]*time.Timer{
					"a": time.NewTimer(time.Minute),
					"b": time.NewTimer(time.Minute),
					"c": time.NewTimer(time.Minute),
				},
			},
			want: true,
		},
		{
			name: "empty",
			fields: fields{
				locks: map[string]*time.Timer{},
			},
			want: false,
		},
		{
			name: "nil",
			fields: fields{
				locks: nil,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &ActivationLock{
				locks: tt.fields.locks,
			}
			if got := a.IsLocked(); got != tt.want {
				t.Errorf("ActivationLock.IsLocked() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestActivationLock_Lock(t *testing.T) {
	defaultLockTimeout = time.Millisecond // Set to low timeouts for testing.
	type args struct {
		name     string
		callback func()
	}
	tests := []struct {
		name    string
		args    args
		waitFor time.Duration
		want    bool
	}{
		{
			name: "simple lock",
			args: args{name: "simple", callback: func() {
				t.Logf("auto unlock")
			}},
			waitFor: time.Microsecond,
			want:    true,
		},
		{
			name: "auto-unlock",
			args: args{name: "simple", callback: func() {
				t.Logf("auto unlock")
			}},
			waitFor: 2 * time.Millisecond,
			want:    false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := NewActivationLock()
			a.Lock(tt.args.name, tt.args.callback)
			time.Sleep(tt.waitFor)
			if got := a.IsLocked(); got != tt.want {
				t.Errorf("ActivationLock.Lock() & IsLocked() = %v, want %v", got, tt.want)
			}
		})
	}
}
