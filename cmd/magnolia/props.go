package magnolia

// Property is a KV type in JCR.
type Property struct {
	Name     string   `json:"name" yaml:"name"`
	Type     string   `json:"type,omitempty" yaml:"type,omitempty"`
	Multiple bool     `json:"multiple,omitempty" yaml:"multiple,omitempty"`
	Values   []string `json:"values,omitempty" yaml:"values,omitempty"`
}

// NewProperty returns an empty property object.
func NewProperty(name string) *Property {
	p := &Property{
		Name:   name,
		Values: make([]string, 0),
	}
	return p
}

// Equals returns true if the names match.
func (p *Property) Equals(other *Property) bool {
	return p.Name == other.Name
}
