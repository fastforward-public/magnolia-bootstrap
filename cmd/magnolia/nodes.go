package magnolia

// A few sentinel values for JCR ...
const (
	contentNodeType = "mgnl:contentNode" // A generic content node.
	userNodeType    = "mgnl:user"        // Is a user node.
	rootNodeType    = "rep:root"         // A root node.
)

// Node is a generic node in JCR. Nodes have properties and can have other nodes
// as children.
type Node struct {
	Name       string      `json:"name,omitempty" yaml:"name,omitempty"`
	Type       string      `json:"type,omitempty" yaml:"type,omitempty"`
	Path       string      `json:"path,omitempty" yaml:"path,omitempty"`
	Identifier string      `json:"identifier,omitempty" yaml:"identifier,omitempty"`
	Properties []*Property `json:"properties,omitempty" yaml:"properties,omitempty"`
	Nodes      []*Node     `json:"nodes,omitempty" yaml:"nodes,omitempty"`
}

// WalkFunc is a func which is used to walk the node tree. On error != nil the
// walk should be stopped.
type WalkFunc func(n *Node) error

// Walk executes f for each node it encounters down the tree starting at itself
// as the root. If f returns an error, the walk stops and relays this error.
func (n *Node) Walk(f WalkFunc) error {
	if err := f(n); err != nil {
		return err
	}
	for _, v := range n.Nodes {
		// Recurse.
		if err := v.Walk(f); err != nil {
			return err
		}
	}
	return nil
}

// Equals returns true if the identifiers of the nodes match.
func (n *Node) Equals(other *Node) bool {
	return n.Identifier == other.Identifier
}

// NewNode makes a generic new node with a name.
func NewNode(name string) *Node {
	n := &Node{
		Name:       name,
		Properties: make([]*Property, 0),
		Nodes:      make([]*Node, 0),
	}
	return n
}

// NewUserNode creates a new user node.
func NewUserNode(name string) *Node {
	n := NewNode(name)
	n.Type = userNodeType
	return n
}

// NewContentNode create a new content node.
func NewContentNode(name string) *Node {
	n := NewNode(name)
	n.Type = contentNodeType
	return n
}
