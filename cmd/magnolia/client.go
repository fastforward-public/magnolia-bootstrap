package magnolia

import (
	"context"
	"fmt"
	"mgnlboot/cmd/api"
	"net/http"
	"net/url"
	"path"
)

const (
	baseURLTpl      = "%s/.rest/%s/" // First %s is the scheme://host part.
	defaultEndpoint = "nodes/v1"     // We use the node endpoint if none given.
)

// Client is a magnolia REST API client.
type Client struct {
	*api.Client

	Endpoint string // Endpoint part in the Magnolia REST API path.
}

// Clone this client with all its settings.
func (c *Client) Clone(opts ...api.Opt) (*Client, error) {
	cli, err := c.Client.Clone(opts...)
	if err != nil {
		return nil, err
	}
	return &Client{
		Client:   cli,
		Endpoint: c.Endpoint,
	}, nil
}

// BaseURL returns the API endpoint url after parsing u.
func BaseURL(address, endpoint string) (*url.URL, error) {
	if endpoint == "" {
		endpoint = defaultEndpoint
	}
	base, err := url.Parse(fmt.Sprintf(baseURLTpl, address, endpoint))
	if err != nil {
		return nil, fmt.Errorf("cannot parse %s as BaseURL, endpoint = %s: %w", address, endpoint, err)
	}

	base.Path = path.Clean(base.Path) + "/"
	return base, nil
}

// Opt is a Magonlia client option.
type Opt func(*Client) error

// OptFunc returns a an Opt if f is an api.Opt.
func OptFunc(f api.Opt) Opt {
	return func(c *Client) error {
		if err := f(c.Client); err != nil {
			return err
		}
		return nil
	}
}

// OptFuncs transforms api.Opts to Opts. See OptFunc.
func OptFuncs(opts []api.Opt) []Opt {
	var out = make([]Opt, 0)
	for _, f := range opts {
		out = append(out, OptFunc(f))
	}
	return out
}

// WithEndpoint sets the REST API endpoint part in the path.
func WithEndpoint(endpoint string) Opt {
	return func(c *Client) error {
		c.Endpoint = endpoint
		return nil
	}
}

// WithBaseURL sets the BaseURL.
func WithBaseURL(address string) Opt {
	return func(c *Client) error {
		base, err := BaseURL(address, c.Endpoint)
		if err != nil {
			return err
		}
		c.BaseURL = base
		return nil
	}
}

// NewClient returns a new Magnolia REST API client.
func NewClient(opts ...Opt) (*Client, error) {
	c, err := api.New()
	if err != nil {
		return nil, err
	}
	cli := &Client{Client: c}
	for _, opt := range opts {
		if err := opt(cli); err != nil {
			return nil, err
		}
	}
	return cli, nil
}

// Get a JCR node with the given depth of recursion, default is 1. The result
// will be filled into *node. If anything different happens, an error is
// returned.
func (c *Client) Get(ctx context.Context, location string, depth int, node *Node) error {
	location = fmt.Sprintf("%s?depth=%d", location, depth)
	req, err := c.NewRequest(ctx, http.MethodGet, location, nil)
	if err != nil {
		return fmt.Errorf("could not create new HTTP request: %w", err)
	}

	_, err = c.Do(ctx, req, node)
	if err != nil {
		return fmt.Errorf("could not execute HTTP request: %w", err)
	}

	return nil
}
