package cmd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"mgnlboot/cmd/api"
	"mgnlboot/cmd/health"
	"mgnlboot/cmd/magnolia"
	"net/url"
	"os"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "start bootstrap in server mode",
	Args: func(cmd *cobra.Command, args []string) error {
		vip := vipers["serve"]
		target := vip.GetString("magnolia")
		if _, err := url.Parse(target); err != nil {
			return fmt.Errorf("could not parse URL %s: %w", target, err)
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		vip := vipers["serve"]

		verbosity := setVerbosity(vip.GetInt("verbosity"))
		logw := logrus.StandardLogger().WriterLevel(logrus.TraceLevel)
		defer logw.Close()
		logrus.Debugf("set logging mode to %s", logrus.GetLevel().String())

		insecure := vip.GetBool("insecure")
		mgnl := vip.GetString("magnolia")

		cli, err := magnolia.NewClient(
			magnolia.OptFunc(api.WithBaseURL(mgnl)),
			magnolia.OptFunc(api.WithInsecure(insecure)),
			magnolia.OptFunc(api.WithVerbosity(verbosity)),
			magnolia.OptFunc(api.WithLogger(log.New(logw, "", 0))),
		)
		if err != nil {
			logrus.Errorf("could not get HTTP client: %s", err)
			return nil
		}
		codes := vip.GetStringSlice("code")
		response := vip.GetStringSlice("response")
		interval := vip.GetDuration("interval")
		maxInterval := vip.GetDuration("max-interval")
		timeout := vip.GetDuration("connect-timeout")
		maxWait := vip.GetDuration("max-wait")
		hcPath := vip.GetString("path")

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		// This is our Magnolia health check.
		op := func() error {
			ctx, cancel := context.WithTimeout(ctx, timeout)
			defer cancel()
			err := httpGet(ctx, cli, hcPath, codes, response)
			if err == nil {
				return nil
			}
			if errors.Is(err, context.Canceled) || errors.Is(err, context.DeadlineExceeded) {
				return backoff.Permanent(err)
			}
			return err
		}

		var n int
		notify := func(err error, d time.Duration) {
			logrus.Debugf("error received, waiting for %s: %s", d, err)
			n++
		}

		healthOpts := []health.Opt{
			health.WithInterval(interval),
			health.WithMaxInterval(maxInterval),
			health.WithTimeout(vip.GetDuration("timeout")),
			health.WithMaxRetries(vip.GetInt("retry")),
			health.WithLogger(log.New(logrus.StandardLogger().WriterLevel(logrus.InfoLevel), "", 0)),
		}

		// Now configure the executor.
		executor, err := configureExecutor(vip, verbosity)
		if err != nil {
			logrus.Fatalf("error configuring executor: %s", err)
		}

		// The executor is our "outer" healthcheck. This way only if the
		// Magnolia health check *and* the bootstrap healthcheck run
		// successfully, we report back a positive health check result.
		var mux sync.Mutex // Protects 'success'
		var success bool
		execOp := func() error {
			mux.Lock()
			defer mux.Unlock()
			if success {
				return nil
			}
			ctx, cancel := context.WithTimeout(ctx, maxWait)
			defer cancel()
			if err := executor.Run(ctx); err != nil {
				return fmt.Errorf("❌ error during execution: %s", err)
			}
			logrus.Infof("🥳 successfully executed all instructions given")
			success = true
			return nil
		}

		check, err := health.NewCheck(
			"magnolia health",
			op,
			append(healthOpts,
				health.WithNotify(notify),
			)...,
		)
		if err != nil {
			logrus.Fatalf("error creating health check: %v", err)
		}

		check2, err := health.NewCheck(
			"bootstrap instructions",
			execOp,
			append(healthOpts,
				health.WithChild(check),
				health.WithNotify(notify),
			)...,
		)
		if err != nil {
			logrus.Fatalf("error creating health check: %v", err)
		}

		check2.Run(ctx)

		address := vip.GetString("listen")
		srv := newServer(ctx, address, check, check)

		if vip.GetBool("use-activation-proxy") {
			if err := srv.withActivationProxy(vip.GetString("magnolia")); err != nil {
				logrus.Fatalf("could not initialize activation proxy: %v", err)
			}
		}

		logrus.Infof("server version %s listening on %s", rootCmd.Version, address)
		go srv.Run()
		for err := range srv.Done {
			logrus.Error(err)
		}

		return nil
	},
}

func configureExecutor(vip *viper.Viper, verbosity int) (*Executor, error) {
	username := vip.GetString("username")
	if username == "" {
		return nil, fmt.Errorf("username was not provided")
	}
	password := vip.GetString("password")
	if password == "" {
		return nil, fmt.Errorf("password was not provided")
	}
	insecure := vip.GetBool("insecure")
	dryrun := vip.GetBool("dry-run")
	mgnl := vip.GetString("magnolia")
	endpoint := vip.GetString("endpoint")

	cli, err := magnolia.NewClient(
		magnolia.WithEndpoint(endpoint),
		magnolia.WithBaseURL(mgnl),
		magnolia.OptFunc(api.WithBasicAuth(username, password)),
		magnolia.OptFunc(api.WithVerbosity(verbosity)),
		magnolia.OptFunc(api.WithInsecure(insecure)),
	)
	if err != nil {
		return nil, fmt.Errorf("could not initialize magnolia client: %w", err)
	}

	instrFile := vip.GetString("instructions")
	if instrFile == "" {
		// No instruction file provided means we don't need any instructions.
		return &Executor{}, nil // Empty executor.
	}

	// Get content of yaml input with modifications.
	file, err := os.Open(instrFile)
	if err != nil {
		return nil, fmt.Errorf("could not open instructions file: %w", err)
	}
	defer file.Close()

	var inst InstructionList
	if err := getInstructionList(&inst, file); err != nil {
		return nil, fmt.Errorf("could not get instructions from file: %w", err)
	}

	executor := NewExecutor(cli, &inst, dryrun)
	return executor, nil
}

func init() {
	v := viper.New()
	initViper(v)
	vipers["serve"] = v

	flags := serveCmd.Flags()

	flags.StringP("listen", "l", ":8765", "Where we listen on")
	flags.StringP("magnolia", "m", "http://magnolia:8080", "Where to find our own Magnolia instance, used for the startup probe check")
	flags.StringP("endpoint", "e", "nodes/v1", "Which API endpoint we're using")
	flags.String("username", "superuser", "User name for API requests")
	flags.String("password", "superuser", "Password for API requests")
	flags.StringP("instructions", "i", "", "Instruction yaml file location")
	flags.CountP("verbosity", "v", "Verbosity level (--v: verbose, --vv: debug, --vvv: trace)")
	flags.Bool("dry-run", false, "Dry run, don't change anything")
	flags.DurationP("max-wait", "w", defaultMaxDuration, "Max time to wait for completion of all tasks.")
	flags.Bool("insecure", false, "Do not verify server's TLS certificates. WARNING: Use in production is not recommended!")
	flags.BoolP("use-activation-proxy", "p", true, "Start activation endpoint proxy for host given in '-m'.")

	// Flags for the health check.
	flags.StringSlice("code", []string{"200-399"}, "Healthcheck: Check for this HTTP response code range. Non-contiguous ranges can be separated by comma.")
	flags.IntP("retry", "r", -1, "Healthcheck: Retry this many times before giving up. Negative values mean 'infinite'.")
	flags.DurationP("interval", "n", time.Millisecond*500, "Healthcheck: Starting retry interval between failed attempts.")
	flags.Duration("max-interval", time.Second*10, "Healthcheck: Max retry interval between failed attempts.")
	flags.Duration("connect-timeout", time.Second*5, "Timeout for a single HTTP operation in probes.")
	flags.DurationP("timeout", "t", time.Second*30, "Timeout before transitioning from success->fail.")
	flags.StringSlice("response", nil, "Regex of response to expect. If all of the regexes match, the probe succeeds.")
	flags.String("path", "/", "Path used to do healthchecks against, e.g. '/.rest/status'")

	if err := v.BindPFlags(flags); err != nil {
		logrus.Fatal(err)
	}

	rootCmd.AddCommand(serveCmd)
}
