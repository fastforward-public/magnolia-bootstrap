package cmd

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"

	"github.com/sirupsen/logrus"
)

var cfgFile string

// A list of viper instances to keep track of suboptions with the same name but
// different meaning.
var vipers = make(map[string]*viper.Viper)

// initViper works around issue #233 in viper:
// https://github.com/spf13/viper/issues/233
func initViper(v *viper.Viper) {
	v.SetEnvPrefix("MGNLBOOT")
	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "mgnlboot",
	Short: "Magonlia bootstrapper",
	Long: `This program uses the Magnolia "REST"-APIs to bootstrap initial data like
	superuser credentials, subscribers and other deployment information detail which
	could be important for the deployment.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(version string) {
	rootCmd.Version = version
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.mgnlboot.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	formatter := &logrus.TextFormatter{}
	formatter.TimestampFormat = time.RFC3339
	formatter.FullTimestamp = true
	logrus.SetFormatter(formatter)

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".mgnlboot" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".mgnlboot")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
