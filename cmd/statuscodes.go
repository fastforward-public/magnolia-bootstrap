package cmd

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type intRange struct {
	Start int
	End   int
}

func (r intRange) Between(in int) bool {
	return r.Start <= in && in <= r.End
}

func (r intRange) String() string {
	return fmt.Sprintf("%d-%d", r.Start, r.End)
}

type intRangeList []intRange

// Sort by start.
func (a intRangeList) Len() int           { return len(a) }
func (a intRangeList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a intRangeList) Less(i, j int) bool { return a[i].Start < a[j].Start }

// Compact intRanges to a contiguous list if possible.
func (a intRangeList) Compact() intRangeList {
	if len(a) < 2 {
		return a
	}
	sort.Sort(a)
	var cont = true
	for i := 1; cont; i++ {
		if a[i-1].End >= a[i].Start-1 {
			// We can merge, ranges are contiguous.
			if a[i-1].End < a[i].End {
				// Only set the end if the next one is bigger.
				a[i-1].End = a[i].End
			}
			if i == len(a)-1 { // We have reached the end.
				return a[:i]
			}
			a = append(a[:i], a[i+1:]...)
			i--
		}
		cont = i < len(a)-1
	}
	return a
}

func (a intRangeList) Between(in int) bool {
	for _, v := range a {
		if v.Between(in) {
			return true
		}
	}
	return false
}

func (a intRangeList) String() string {
	ii := make([]string, len(a))
	for k, v := range a {
		ii[k] = v.String()
	}
	return strings.Join(ii, ", ")
}

func (a intRangeList) MarshalJSON() ([]byte, error) {
	out := make([]string, len(a))
	for k, v := range a {
		out[k] = v.String()
	}
	return json.Marshal(out)
}

func (a *intRangeList) UnmarshalJSON(b []byte) error {
	var str []string
	err := json.Unmarshal(b, &str)
	if err != nil {
		return err
	}

	*a, err = parseCodeRanges(str)
	if err != nil {
		return err
	}

	return nil
}

func parseCodeRanges(in []string) (intRangeList, error) {
	var li = make(intRangeList, len(in))
	for k, v := range in {
		ii := strings.Split(v, "-")
		if len(ii) > 2 || len(ii) < 1 {
			return nil, fmt.Errorf("could not parse range: %s in range list %s", v, in)
		}
		if len(ii) == 1 {
			ii = append(ii, ii[0])
		}
		a, err := strconv.Atoi(ii[0])
		if err != nil {
			return nil, fmt.Errorf("could not parse int: %w", err)
		}
		b, err := strconv.Atoi(ii[1])
		if err != nil {
			return nil, fmt.Errorf("could not parse int: %w", err)
		}
		li[k] = intRange{
			Start: a,
			End:   b,
		}
	}
	return li, nil
}
