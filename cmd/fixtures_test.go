package cmd

import (
	"io/ioutil"
	"testing"
)

func fixture(path string, t *testing.T) string {
	b, err := ioutil.ReadFile("testdata/fixtures/" + path)
	if err != nil {
		t.Errorf("could not load test data from %s: %v", path, err)
		return ""
	}

	return string(b)
}
