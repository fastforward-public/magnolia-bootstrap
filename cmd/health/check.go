package health

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"time"

	"github.com/cenkalti/backoff/v4"
)

const (
	// ContentType is the MIME content type of the health check result.
	ContentType = "application/health+json"

	// StatusPass means everything is ok.
	StatusPass = "pass"
	// StatusFail means there's a problem.
	StatusFail = "fail"
)

// Defaults ...
const (
	DefaultInterval    = 10 * time.Second // Starting interval.
	DefaultMaxInterval = 1 * time.Minute  // Max interval.
	DefaultTimeout     = 5 * time.Minute  // Timeout before transitioning to next state.
)

// Status of the last check result.
type Status string

func (s Status) String() string {
	switch s {
	case StatusPass:
		return "👍"
	default:
		return "👎"
	}
}

// MarshalJSON always emits fail in case Status is the zero value.
func (s Status) MarshalJSON() ([]byte, error) {
	var o string
	switch s {
	case StatusPass:
		o = StatusPass
	default:
		o = StatusFail
	}
	return json.Marshal(o)
}

// Result is s healthCheck response according to RFC draft spec for health
// checks: https://tools.ietf.org/id/draft-inadarei-api-health-check-01.html
type Result struct {
	Status      Status   `json:"status"`
	Version     string   `json:"version,omitempty"`
	Notes       []string `json:"notes,omitempty"`
	Output      string   `json:"output,omitempty"`
	ServiceID   string   `json:"serviceId,omitempty"`
	Description string   `json:"description,omitempty"`
}

func (r Result) String() string {
	if r.Output == "" {
		if r.Status == "" {
			r.Status = StatusFail
		}
		return fmt.Sprintf("status=%s", r.Status)
	}
	return fmt.Sprintf("status=%s, output=%s", r.Status, r.Output)
}

// Check checks for health, it can be cascaded.
type Check struct {
	Name        string                           // The check's name, optional.
	ctx         context.Context                  // Checking stops after this.
	interval    time.Duration                    // Starting interval.
	maxInterval time.Duration                    // Max interval.
	timeout     time.Duration                    // Timeout before transitioning to next state.
	maxRetries  int                              // Max number of retries for health check.
	check       func() error                     // Our health check to execute.
	notify      func(err error, d time.Duration) // Will be called on each execution of 'check'.
	// First check this, if it is ok, we can do our own check. If it is nil,
	// ignore it. This way we can cascade health checks.
	childCheck *Check

	cond  *sync.Cond  // Protects state and is used for the Wait() func.
	state Result      // The current status of the check.
	once  sync.Once   // Make sure this check only runs once.
	log   *log.Logger // Log to here.
}

// Opt is a healthcheck option.
type Opt func(*Check) error

// NewCheck returns a freshly initialized health check.
func NewCheck(name string, check func() error, opts ...Opt) (*Check, error) {
	var mux sync.Mutex
	h := &Check{
		Name:        name,
		check:       check,
		interval:    DefaultInterval,
		maxInterval: DefaultMaxInterval,
		timeout:     DefaultTimeout,
		log:         log.New(ioutil.Discard, "", 0),
		cond:        sync.NewCond(&mux),
		//state:       Result{Status: StatusFail},
	}

	for _, opt := range opts {
		if err := opt(h); err != nil {
			return nil, err
		}
	}

	return h, nil
}

// WithStatus optionally sets the starting status before we Run().
func WithStatus(startWith Result) Opt {
	return func(c *Check) error {
		c.state = startWith
		return nil
	}
}

// WithInterval sets the starting interval used on retries.
func WithInterval(interval time.Duration) Opt {
	return func(c *Check) error {
		c.interval = interval
		return nil
	}
}

// WithMaxInterval sets the maximum interval to reach by the exponential backoff
// mechanism.
func WithMaxInterval(interval time.Duration) Opt {
	return func(c *Check) error {
		c.maxInterval = interval
		return nil
	}
}

// WithTimeout sets the timeout until the state is changed from fail to pass or
// from pass to fail.
func WithTimeout(timeout time.Duration) Opt {
	return func(c *Check) error {
		c.timeout = timeout
		return nil
	}
}

// WithMaxRetries sets the amount of retries before giving up.
func WithMaxRetries(max int) Opt {
	return func(c *Check) error {
		c.maxRetries = max
		return nil
	}
}

// WithChild adds 'child' as a dependency to this health check.
func WithChild(child *Check) Opt {
	return func(c *Check) error {
		c.childCheck = child
		return nil
	}
}

// WithNotify sets the funcion which is called after each retry. 'err' is the
// error which triggered the retry and 'd' the duration we will wait until the
// next retry.
func WithNotify(f func(err error, d time.Duration)) Opt {
	return func(c *Check) error {
		c.notify = f
		return nil
	}
}

// WithLogger sets the logger for the health check. In case no logger is set, it
// does not log at all.
func WithLogger(l *log.Logger) Opt {
	return func(c *Check) error {
		if l == nil {
			return fmt.Errorf("logger should not be nil")
		}
		c.log = l
		return nil
	}
}

// Run the health check continuously until the context expires. Does not block.
func (h *Check) Run(ctx context.Context) {
	h.once.Do(func() {
		h.ctx = ctx
		go func() {
			for state := stateFailure; state != nil; {
				state = state(h)
			}
		}()
		// Start the child health check too.
		if h.childCheck != nil {
			h.childCheck.Run(ctx)
		}
	})
}

func (h *Check) timeTrack(start time.Time, stateName string) {
	d := time.Since(start)
	h.log.Printf("%s: time spent in %s state: %s", h.Name, stateName, d)
}

// stateFn is a recursive definition. It is part of our state machine to
// asynchronously determine and communicate state.
type stateFn func(*Check) stateFn

func (h *Check) changeStateTo(next Result) {
	h.cond.L.Lock()
	defer h.cond.L.Unlock()
	cur := h.state
	defer h.log.Printf("%s: transitioning from %s to %s", h.Name, cur, next)

	h.state = next
	if next.Status == StatusPass {
		h.cond.Broadcast()
	}
}

func stateSuccess(h *Check) stateFn {
	ctx := h.ctx
	defer h.timeTrack(time.Now(), "success")

	for {
		// Start probing target.
		if err := h.do(ctx); err != nil {
			// We transition to failure state.
			if h.log != nil {
				h.log.Print(err.Error())
			}
			h.changeStateTo(Result{Status: StatusFail})
			return stateFailure
		}
		select {
		case <-ctx.Done():
			return nil // Stop if parent ctx is done.
		default:
		}
		time.Sleep(h.maxInterval)
	}
}

func stateFailure(h *Check) stateFn {
	ctx := h.ctx
	defer h.timeTrack(time.Now(), "failure")

	for {
		// Start probing target.
		err := h.do(ctx)
		if err == nil {
			// We transition to success state.
			h.changeStateTo(Result{Status: StatusPass})
			return stateSuccess
		}
		if h.log != nil {
			h.log.Print(err.Error())
		}
		select {
		case <-ctx.Done():
			return nil // Stop if parent ctx is done.
		default:
		}
		time.Sleep(h.maxInterval)
	}
}

func (h *Check) do(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, h.timeout)
	defer cancel()

	if h.childCheck != nil {
		if !h.childCheck.IsSuccess() {
			// There's no point in checking if our child failed.
			return fmt.Errorf("child check %q failed, skipping", h.childCheck.Name)
		}
	}

	var bo backoff.BackOff
	b := backoff.NewExponentialBackOff()
	b.InitialInterval = h.interval
	b.MaxInterval = h.maxInterval
	retry := h.maxRetries
	if retry > 0 {
		bo = backoff.WithMaxRetries(b, uint64(retry))
	} else {
		bo = b
	}
	boctx := backoff.WithContext(bo, ctx)
	if err := backoff.RetryNotify(h.check, boctx, h.notify); err != nil {
		return err
	}
	return nil
}

// Wait until successful or ctx is done. Note that calling IsSuccess() followed
// by Result() is racy. Wait guarantees to return the result at the time the
// check was successful.
func (h *Check) Wait(ctx context.Context) (Result, error) {
	h.cond.L.Lock()
	defer h.cond.L.Unlock()
	for h.state.Status != StatusPass {
		select {
		case <-ctx.Done():
			return Result{}, ctx.Err()
		default:
			h.cond.Wait()
		}
	}
	return h.state, nil
}

// IsSuccess immediately returns true if successful, false otherwise.
func (h *Check) IsSuccess() bool {
	h.cond.L.Lock()
	defer h.cond.L.Unlock()
	return h.state.Status == StatusPass
}

// Result immediately returns the last result.
func (h *Check) Result() Result {
	h.cond.L.Lock()
	defer h.cond.L.Unlock()
	return h.state
}
